(function ($) {

  "use strict";

  $.ajax({
      url : "/themes/frontendunited/img/spotlight.svg",
      dataType: "text",
      success : function (data) {
          $('<div class="spotlight">'+data+'</div>').prependTo('#block-views-block-speakers-block-1 .speakers');
          hoverfunc();
      }
  });


  // Mobile navigation.
  $(".primary-navigation__toggle").mousedown(function() {
    $( ".menu--main ul.menu").toggle('fast');
  });


  var light_pos;
  var lightbeam;
  var svg;
  function hoverfunc(){
    $(".speaker").first().addClass("active");
    svg = $(".spotlight");
    lightbeam = $("#Laag_2");
    light_pos = $(lightbeam).offset();

    $(".speaker").mouseenter(function(){
      recalc(this);
      $(".speaker").removeClass("active");
      $(this).addClass("active");
    });
  }

  function recalc(speaker){
    var light_center = light_pos.left + ($(lightbeam)[0].getBoundingClientRect().width/2);
    var speaker_center = $(speaker).offset().left + ($(speaker).outerWidth()/2);
    var diff = speaker_center - light_center;

    $(svg).css({
      "transform": "translate3d("+diff+"px, 0px, 0px)"
    }).addClass('flicker').delay(3000).removeClass('flicker');
  }

})(jQuery);
