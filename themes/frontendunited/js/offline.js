(function ($) {
  "use strict";
  $(document).ready(function() {
    $('.speaker--tiny-teaser').on('click', function(e) {
      $(this).toggleClass('js-open').find('.field--name-body').slideToggle();
    });

    $('.session--teaser').on('click', function(e) {
      $(this).toggleClass('js-open').find('.field--name-field-subject').slideToggle();
    });

    $("a").on('click',function(e) {
      //var baseUrl = window.location.protocol + "//" + window.location.hostname + Drupal.settings.basePath;
      //if (this.href.indexOf(baseUrl) == 0) {
        e.preventDefault();
        window.location = $(this).attr("href");
      //}
    });
  });
})(jQuery);
